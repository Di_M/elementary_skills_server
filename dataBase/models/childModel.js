
import {Schema, Types, model} from 'mongoose';


const childSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: true
  },
  gender: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  totalScoreLesson: {
    type: Number,
    required: true
  },
  totalQuizPercent: {
    type: Number,
    required: true
  },
  totalPoints: {
    type: Number,
    required: true
  },
  courses: {
    type: Array
  },
  password: {
    type: String,
    required: true
  },
  parentId: {
      type: Types.ObjectId,
      ref: 'Parent'
  }
});

//module.exports = model('Children', childSchema);
export default model('Children', childSchema);

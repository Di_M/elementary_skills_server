import { Router } from "express";
import verifyToken from "../middleware/token-verification";
import Children from "../../dataBase/models/childModel";
import Course from "../../dataBase/models/course/courseModel";
import Section from "../../dataBase/models/course/courseSectionModel";
import ChildLesson from "../../dataBase/models/childLessonModel";
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');

const router = Router();

export default app => {
    app.use('/child', router);

    router.post('/add',
        verifyToken,
        async (request, response) => {

            console.log('Request.body:', request.body);
            console.log(request.user);

            try {
                const errors = validationResult(request);
                if (!errors.isEmpty()){
                    return response.status(400).json({
                        errors:errors.array(),
                        message:'[1] Wrong Register Data'
                    })
                }

                const { name, age, gender, password } = request.body;

                // User object and role properties were added in verifyToken middleware and now available in every authorized route
                const { _id } = request.user;

                // Check if such child already exists (nickname should be unique):
               // const childWithName = await Children.findOne({ name });

                const childWithName = await Children.findOne({ name });
                if (childWithName) return response.status(400).json({message: 'User with such name already exists'});
                // Hash password:
                const hashedPassword = await bcrypt.hash(password, 10);
                // // Save to DB:
                const child = new Children({
                    name,
                    age,
                    gender,
                    avatar: '',
                    totalScoreLesson: 0,
                    totalQuizPercent: 0,
                    totalPoints: 0,
                    courses: [],
                    password: hashedPassword,
                    parentId: _id
                });

                try {
                    await child.save();

                } catch (error){
                    console.log(error)
                }

                response.status(201).json({message: 'Child was added', data: child});

            } catch (error) {
                return response.status(500).json({message: 'Impossible to add a child: ', error});
            }
        }
    );

    router.delete('/:id',
        verifyToken,
        async (request, response) => {

            const {id} = request.params;
            console.log('delete request.body -->', id);
            console.log('delete request.user -->', request.user);

            try {
                const errors = validationResult(request);
                if (!errors.isEmpty()){
                    return response.status(400).json({
                        errors:errors.array(),
                        message:'[1] Wrong Register Data'
                    })
                }

                await Children.deleteOne({ _id: id }, function (err) {
                    if(err) return response
                        .status(500)
                        .json({message: 'No such child'});
                });

                response.status(201).json({message: 'Child was deleted'});

            } catch (error) {
                return response.status(500).json({message: 'Impossible to delete a child: ', error});
            }
        }
    );


    router.put('/edit/:id',
        verifyToken,
        async (request, response) => {

            try {
                const {id} = request.params;
                const {name, age, gender} = request.body;

                //Find child
                const child = await Children.findOne({ _id: id });
                if (!child) return response.status(400).json({message: "Global Error. DataBase don't have this child"});

                // Check edited name, age, gender
                if(child.name === name && child.age === age && child.gender === gender) return response
                        .status(201)
                        .json({message: 'Nothing was changed'});


                // Check the same name
                if(child.name !== name) {
                    const childSameName = await Children.findOne({ name });
                    if (childSameName) return response
                        .status(400)
                        .json({message: 'Child with such name already exists. Please type another'});
                }

                // Returned value: changed child:
                const changedChild = await Children.findByIdAndUpdate(
                    {_id: id},
                    {name, age, gender},
                    {
                        new: true,
                        upsert: true
                    });

                return response.status(201).json({message: 'Child info was changed'});

            } catch(error) {

                return response.status(500).json({message: 'Impossible to change a child info: ', error});
            }
        }
    );

    router.put('/add-course',
        verifyToken,
        async (request, response) => {
            const { assignedChildrenList, courseId } = request.body;

            try {
                const errors = validationResult(request);

                if (!errors.isEmpty()){
                    return response.status(400).json({
                        errors:errors.array(),
                        message:'[2] Wrong  POST Data'
                    })
                }

                const { _id: course, courseSections } = await Course
                    .findById(courseId, { _id: 1, courseSections: 1 });

                const children = await Children.find({ _id: { $in: assignedChildrenList } });

                for (const child of children) {
                  if (child.courses && child.courses.includes(course)) {
                    response.status(200).send({message: "Warning: course has already been assigned!"});
                  } else {

                    await Children.updateOne(
                        { _id: child._id },
                        { $push: {courses: course } }
                    );

                    const courseSectionLessons = await Section
                        .find({ _id: { $in: courseSections }}, { _id: 0, courseSectionLessons: 1 });
                    const lessons = courseSectionLessons
                        .map(sectionLessons => sectionLessons.courseSectionLessons)
                        .flat();
                    const childLessons = lessons.map(lessonId => ({
                        childId: child._id,
                        lessonId: lessonId,
                        lessonIsFinished: false,
                        created_at: Date.now(),
                        updated_at: Date.now(),
                    }));
                    await ChildLesson.insertMany(childLessons);
                  }
                }

              response.status(200).send({
                    message: "Course was successfully added"
                });
            } catch (error) {
                response.status(500).send({message: error});
            }
        }
    );

    router.put('/remove-course',
        verifyToken,
        async (request, response) => {
          const { childId, courseId } = request.body;

          try {
            const { courses } = await Children.findById(childId, { courses: 1 });

            if (courses && courses.includes(courseId)) {
              const newCourses = courses.filter(course => course != courseId);
              await Children.updateOne(
                { _id: childId },
                { $set: { courses: newCourses } }
              );

              const { courseSections } = await Course
                .findById(courseId, { _id: 0, courseSections: 1 })
                .populate({
                  path: 'courseSections',
                  select: 'courseSectionLessons',
                });
              const lessons = courseSections
                .map(sectionLessons => sectionLessons.courseSectionLessons)
                .flat();
              await ChildLesson.deleteMany({
                lessonId: { $in: lessons }
              });

            } else {
              throw new Error("No such course found in Child courses");
            }

            response.status(200).send({
              message: "Course was successfully removed"
            });
          } catch (error) {
            response.status(500).send({message: error});
          }

        }
    );
}

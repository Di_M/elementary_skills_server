import {Schema, Types, model} from 'mongoose';

const childLessonSchema = new Schema({
    childId: {
        type: Types.ObjectId,
        required: true,
        ref: 'Children'
    },
    lessonId: {
        type: Types.ObjectId,
        required: true,
        ref: 'Lesson'
    },
    lessonIsFinished: {
        type: Boolean,
        required: true,
        default: false,
    },
    lessonEarnedPoints: {
        type: Number,
        required: true,
        default: 0,
    },
    created_at: {
        type: Date,
        required: true,
        default: new Date()
    },
    updated_at: {
        type: Date,
        required: true,
        default: new Date()
    }
});

export default model('ChildLesson', childLessonSchema);

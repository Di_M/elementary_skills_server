import { Router } from 'express';
const bcrypt = require('bcryptjs');
const {check, validationResult}=require('express-validator')
import Parent from "../../dataBase/models/parentModel";

const route = Router();

export default app => {
  app.use('/sign-up', route);

  route.post('/',
    async (request, response) => {

      try {
        const errors = validationResult(request)
        if (!errors.isEmpty()){
          return response.status(400).json({
            errors:errors.array(),
            message:'[2] Wrong  USER VALIDATION Data'
          })
        }
        const { name, email, password } = request.body;

        // Check if such parent already exists (email and nickname should be unique):
        const userWithEmail = await Parent.findOne({ email });
        const userWithNickName = await Parent.findOne({ name });
        if (userWithEmail) return response.status(400).json({message: 'User with such email already exists'});
        if (userWithNickName) return response.status(400).json({message: 'User with such name already exists'});

        // Hash password:
        const hashedPassword = await bcrypt.hash(password, 10);

        // Save to DB:
        const parent = await new Parent({
          name,
          email,
          password: hashedPassword,
        });
        await parent.save();

        return response.status(201).json({message: 'User was created'});

      } catch(error) {
        return response.status(500).json({message: 'Impossible to create a user: ', error});
      }
    }
  );
}

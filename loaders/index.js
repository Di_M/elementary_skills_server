import expressLoader from './express';
import mongoLoader from './mongoDB';

export default async ({expressApp}) => {

    //Initialization and connection Express
    await expressLoader({app: expressApp});
    console.log(
        '################################################' +
        '\n🛡Express was loaded🛡' +
        '\n################################################');


    // Connection with Data Base on MongoDB
    await mongoLoader();
    console.log('################################################' +
        '\n***MongoDB was loaded***' +
        '\n################################################');
}
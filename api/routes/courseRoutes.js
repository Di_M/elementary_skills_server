import { Router } from 'express';
import verifyToken from '../middleware/token-verification';
const {check, validationResult}=require('express-validator')
import Course from "../../dataBase/models/course/courseModel";

const router = Router();

export default app => {
    app.use(`/course`, router);

    router.get('/list',
        verifyToken,
        async (request, response) => {
            try {
                console.log(request.body)
                const errors = validationResult(request)

                if (!errors.isEmpty()){
                    return response.status(400).json({
                        errors:errors.array(),
                        message:'[1] Invalid registration data'
                    })
                }

                const courses = await Course.find({}, { quizSection: 0, courseSections: 0});

                response.status(200).json({
                    courses
                })
            } catch (error) {
                response.status(500).send({message: error})
            }
        }
    );


    router.get('/list/details',
        verifyToken,
        async (request, response) => {
            try {
                const courses = await Course
                    .find({}, { quizSection: 0 })
                    .populate({
                        path: 'courseSections',
                        populate: { path: 'courseSectionLessons' }
                    });

                response.status(200).json({
                    courses
                })
            } catch (error) {
                response.status(500).send({message: error})
            }
        }
    );



    router.get('/quiz/:courseId',
        verifyToken,
        async (request, response) => {
            const { courseId } = request.params;
            
            try {
                const quiz = await Course
                    .findOne(
                        { _id: courseId },
                        { quizSection: 1, }
                    );
                    
                response.status(200).json({
                    quiz,
                });
            } catch (error) {
                response.status(500).send({message: error})
            }
        }
    );
    router.get('/view/:courseId', 
        verifyToken,
        async (request, response) => {
            const { courseId } = request.params;

            try {
                const course = await Course
                    .findOne({ _id: courseId }, {quizSection: 0})
                    .populate({
                        path: 'courseSections',
                        populate: { path: 'courseSectionLessons' }
                    });
                    
                response.status(200).json({
                    course,
                    courseId,
                });
            } catch (error) {
                response.status(500).send({message: error})
            }
        }
    );
}

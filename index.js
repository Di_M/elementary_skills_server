import express from 'express';
import cors from 'cors';

import appLoader from './loaders';
import config from './config';

/**
 * @desc Create function for start our Server
 **/
async function startServer() {
    const app = express();

    const allowedOrigins = [
        // Note: it should be exactly the same host you use on your side
        'http://localhost:63342',
        'http://localhost:3000'
    ];

    app.enable('trust proxy');

    // app.use(cors({
    //     origin: function (origin, callback) {
    //         // allow requests with no origin
    //         // (like mobile apps or curl requests)
    //         if (!origin) {
    //             return callback(null, true);
    //         }
    //
    //         if (allowedOrigins.indexOf(origin) === -1) {
    //             const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
    //
    //             return callback(new Error(msg), false);
    //         }
    //
    //         return callback(null, true);
    //     },
    //     credentials: true
    // }));

    app.use(cors());

    //our loaders, like MongoDB and Express
    await appLoader({expressApp: app});

    //Listener port which we use
    app.listen(config.port, err => {
        if(err) {
            process.exit(1);
            return;
        }

        console.log(`Server listening on port: ${process.env.PORT || 3000}`);
    });
}

startServer();

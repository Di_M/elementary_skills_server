import {Router} from 'express';
import verifyToken from '../middleware/token-verification';
import Parent from '../../dataBase/models/parentModel';

const bcrypt = require('bcryptjs');
const route = Router();

export default app => {
    app.use(`/change-password`, route);

    route.put('/',
        verifyToken,
        async (request, response) => {

            try {
                // user property was added to request in verifyToken middleware. Either child or parent.
                const {_id} = request.user;
                const {password, oldPassword} = request.body;

                const parent = await Parent.findOne({ _id });
                if (!parent) return response.status(400).json({message: 'Global Error. Deleted parent from DataBase'});


                // Returned value: boolean:
                const doesPassMatch = await bcrypt.compare(oldPassword, parent.password);
                if (!doesPassMatch) return response.status(400).json({message: 'Invalid password'});


                //Hash new password
                const hashedPassword = await bcrypt.hash(password, 10);


                // Returned value: changed parent:
                const changedParent = await Parent.findByIdAndUpdate(
                    {_id},
                    {"password": hashedPassword},
                    {
                        new: true,
                        upsert: true
                });

                console.log('Parent -->', changedParent);

                return response.status(201).json({message: 'Password was changed'});

            } catch(error) {

                return response.status(500).json({message: 'Impossible to change a password: ', error});
            }
        }
    );
}
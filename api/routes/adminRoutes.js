import {Router} from 'express';
import verifyToken from '../middleware/token-verification';
import Parent from '../../dataBase/models/parentModel';
import Children from "../../dataBase/models/childModel";
const {check, validationResult}=require('express-validator')

const route = Router();

export default app => {
    app.use(`/account/:id`, route);

    route.get('/',
        verifyToken,
        async (request, response) => {

            console.log('REQUEST BODY:account:id', request.body)
            const errors = validationResult(request)
            if (!errors.isEmpty()){
                return response.status(400).json({
                    errors:errors.array(),
                    message:'[4] Problem with account ID'
                })
            }

          // User object and role properties were added in verifyToken middleware and now available in every authorized route
          const { _id } = request.user;
          const { role } = request;

          if (role === 'parent') {

            const parent = await Parent.findOne({ _id });
            if (!parent) return response.status(400).json({message: 'No such user'});

            const { name, email } = parent;
            return response.status(200).json({ name, email, role })

          } else if (role === 'child') {

            const child = await Children.findOne({ _id });
            if (!child) return response.status(400).json({message: 'No such user'});

            const { name } = child;
            return response.status(200).json({ name, role })
          }
        }
    );
  route.get('/children',
    verifyToken,
    async (request, response) => {

      const { _id } = request.user;

        const children = await Children.find({ parentId: _id });

        return response.status(200).json({ children })

    }
  );
}


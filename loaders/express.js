import routes from '../api';
import bodyParser from 'body-parser';

export default ({app}) => {

    app.enable('trust proxy');

    // Middleware that transforms the raw string of req.body into json
    app.use(bodyParser.json());

    app.use('/', routes());

    /// catch 404 and forward to error handler
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With,content-type,Accept,content-type,application/json');
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
}